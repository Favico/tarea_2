"""-----------------------Autor Christian Condoy-------------------------------
---------------------Email christian.condoy@unl.edu.ec--------------------------
Ejercicio 2: Reescribe el programa del salario usando try y except, de modo que el
programa sea capaz de gestionar entradas no numéricas con elegancia, mostrando
un mensaje y saliendo del programa. A continuación se muestran dos ejecuciones
del programa:"""
try:
    horas = int(input("Ingrese el número de Horas Trabajadas: "))

    tarifa = float(input("Ingrese el valor por Hora Trabajada: "))

    if horas > 40:
        horast = horas - 40
        horasex = (horast * 1.5) * tarifa
        total1 = (40 * tarifa) + horasex
        print(f"Salario a Pagar:{total1}")
    else:
        total2 = horas * tarifa
        print(f"Salario a Pagar: {total2}")

except:
            print(f"Error, Ingrese un número valido")

