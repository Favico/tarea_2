"""-----------------------Autor Christian Condoy-------------------------------
---------------------Email christian.condoy@unl.edu.ec--------------------------
Ejercicio 1: Reescribe el programa del cálculo del salario para darle al empleado
1.5 veces la tarifa horaria para todas las horas trabajadas que excedan de 40."""

horas = int(input("Ingrese el número de Horas Trabajadas: "))

tarifa = float(input("Ingrese el valor por Hora Trabajada: "))

if horas > 40:
    horast = horas - 40
    horasex = (horast * 1.5) * tarifa
    total1 = (40 * tarifa) + horasex
    print(f"Salario a Pagar:{total1}")
else:
    total2 = horas * tarifa
    print(f"Salario a Pagar: {total2}")
